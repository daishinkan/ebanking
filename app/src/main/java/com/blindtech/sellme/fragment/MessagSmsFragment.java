package com.blindtech.sellme.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blindtech.sellme.R;
import com.blindtech.sellme.adapter.MessageAdapter;
import com.blindtech.sellme.models.Message;

import java.util.ArrayList;
import java.util.List;


public class MessagSmsFragment extends Fragment {

    private RecyclerView rv;
    private List<Message> messages;
    private MessageAdapter adapter;

    public MessagSmsFragment() {
        // Required empty public constructor
    }

    public static MessagSmsFragment newInstance() {
        return new MessagSmsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_messag_sms, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null) {

            rv = view.findViewById(R.id.rv);


            messages = new ArrayList<>();

           /* messages.add(new Message("saitama", "le papa de poulos est parti en retraite depuis des annees", "25/04/2010"));
            messages.add(new Message("bob", "le papa de poulos est parti en retraite depuis des annees", "25/04/2010"));
            messages.add(new Message("Ghost", "le papa de poulos est parti en retraite ", "25/04/2010"));
            messages.add(new Message("bob", "le papa de poulos est parti en retraite depuis des annees", "25/04/2010"));
            messages.add(new Message("LeDivinOKLM", "le papa de poulos est parti en retraite depuis des annees", "25/04/2010"));*/


            setAdapter();
        }
    }

    private void setAdapter() {
        adapter = new MessageAdapter(getContext(), messages);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        rv.setAdapter(adapter);
    }
}
