package com.blindtech.sellme.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.blindtech.sellme.R;
import com.blindtech.sellme.activities.SettingsActivity;
import com.blindtech.sellme.adapter.ArticleAdapter;
import com.blindtech.sellme.api.UserHelper;
import com.blindtech.sellme.models.Article;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FeedsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FeedsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FeedsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private Toolbar toolbar;
    private RecyclerView rv;
    private List<Article> articles;
    private ArticleAdapter adapter;

    @BindView(R.id.feed_fragment_text_view_account_number)
    TextView textViewAccountNumber;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FeedsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FeedsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FeedsFragment newInstance(String param1, String param2) {
        FeedsFragment fragment = new FeedsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        ButterKnife.bind(getActivity()); //Configure Butterknife
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_feeds, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbar = view.findViewById(R.id.toolbar);

//        toolbar.setTitle("New Feeds");
        toolbar.setTitle("Accueil");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        toolbar.setBackgroundColor(getResources().getColor(R.color.purple));
        toolbar.inflateMenu(R.menu.main_menu);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_settings:
                        startActivity(new Intent(getActivity(), SettingsActivity.class));
                }
                return true;
            }
        });

        if (getActivity() != null) {

//            rv = view.findViewById(R.id.recycler);

            articles = new ArrayList<>();

            /*articles.add(new Article("Cent facettes de Mr Diamond", "Adidas", "roman",2500, R.drawable.basket,"kakou77"));
            articles.add(new Article("Cop air", "ADIDAS", "shoes",12000, R.drawable.basket,"ibroZ"));
            articles.add(new Article("Madona", "CHANEL", "shoes",22000, R.drawable.shoes,"Daam's"));
            articles.add(new Article("Cent facettes de Mr Diamond", "Addidas", "roman",2500, R.drawable.shoes,"kakou77"));
            articles.add(new Article("Cop air", "ADDIDAS", "shoes",12000, R.drawable.basket,"kakou77"));
            articles.add(new Article("Madona", "CHANEL", "shoes",22000, R.drawable.shoes,"Daam's"));*/

//            setAdapter()    ;
        }

    }

    private void setAdapter() {
        adapter = new ArticleAdapter(getContext(), articles);
        rv.setLayoutManager(new GridLayoutManager(getContext(), 2));
        rv.setAdapter(adapter);
    }

    // --------------------
    // ACTIONS
    // --------------------

    /*@OnClick(R.id.feed_fragment_add_button)
    public void onClickAddButton() {
        this.updateUsernameInFirebase();
    }*/

    //@OnClick(R.id.feed_fragment_card_view_first)
    public void onClickLoginButton() {
        Toast.makeText(getContext(), "Action réussie", Toast.LENGTH_SHORT).show();
    }

    // 3 - Update User Username
   /* private void updateUsernameInFirebase() {

        Double accountNumber = Double.valueOf(this.textInputEditTextAccountNumber.getText().toString());
        System.out.println("Ok");
        if (this.getCurrentUser() != null) {
            System.out.println("Ok");
                UserHelper.updateAccount(accountNumber, this.getCurrentUser().getUid())
                        .addOnFailureListener(this.onFailureListener())
                        .addOnSuccessListener(this.onSuccessListener());

        }
    }*/

    // --------------------
    //  UTILS
    // --------------------

    @Nullable
    protected FirebaseUser getCurrentUser() {
        return FirebaseAuth.getInstance().getCurrentUser();
    }

    protected Boolean isCurrentUserLogged() {
        return (this.getCurrentUser() != null);
    }

    // --------------------
    //  ERROR HANDLER
    // --------------------

    protected OnFailureListener onFailureListener() {
        return new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getActivity(), getString(R.string.error_unknown_error), Toast.LENGTH_LONG).show();
            }
        };
    }

    protected OnSuccessListener<Void> onSuccessListener() {
        return new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getActivity(), getString(R.string.success_message), Toast.LENGTH_LONG).show();
            }
        };
    }

}
