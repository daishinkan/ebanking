package com.blindtech.sellme.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.blindtech.sellme.R;
import com.blindtech.sellme.fragment.AccountFragment;
import com.blindtech.sellme.fragment.FeedsFragment;
import com.blindtech.sellme.fragment.MessageFragment;
import com.blindtech.sellme.fragment.SearchFragment;
import com.blindtech.sellme.fragment.TransferFragment;
import com.blindtech.sellme.help_chat.HelpChatActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HomeActivity extends AppCompatActivity implements
        FeedsFragment.OnFragmentInteractionListener,
        SearchFragment.OnFragmentInteractionListener,
        TransferFragment.OnFragmentInteractionListener,
        MessageFragment.OnFragmentInteractionListener,
        AccountFragment.OnFragmentInteractionListener {


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {

        switch (item.getItemId()) {
            case R.id.navigation_feed:
                replace_fragment(FeedsFragment.newInstance("", ""));
                return true;
            case R.id.navigation_search:
                replace_fragment(SearchFragment.newInstance("", ""));
                return true;
            case R.id.navigation_new:
                /*startActivity(new Intent(HomeActivity.this, NewFeedActivity.class));
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                return false;*/
                replace_fragment(TransferFragment.newInstance("", ""));
                return true;
            case R.id.navigation_message:
                startActivity(new Intent(this, HelpChatActivity.class));
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                return false;
            case R.id.navigation_profil:
                replace_fragment(AccountFragment.newInstance("", ""));
                return true;
        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        replace_fragment(FeedsFragment.newInstance("", ""));

    }

    public void replace_fragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, fragment);
        transaction.commit();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

}
