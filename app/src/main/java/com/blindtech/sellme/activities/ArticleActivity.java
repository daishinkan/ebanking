package com.blindtech.sellme.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.blindtech.sellme.R;

public class ArticleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
    }
}
