package com.blindtech.sellme.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.blindtech.sellme.R;
import com.blindtech.sellme.helper.InputValidation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActivity extends AppCompatActivity {

    String TAG;
    Toolbar toolbar;
    EditText etUsername, etEmail, etPassword;
    TextInputLayout tilUsername, tilEmail, tilPassword;
    TextView btnSignUp;

    InputValidation validation;

    private FirebaseAuth mAuth;

    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initView();

        mAuth = FirebaseAuth.getInstance();

        mProgress = new ProgressDialog(this);
        validation = new InputValidation(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Sign Up");

        toolbar.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_arrow));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SignUp();
            }
        });

    }

    private void SignUp() {

        String username = etUsername.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (!validation.isEditTextFilled(etUsername, tilUsername)) {
            return;
        }
        if (!validation.isEditTextEmail(etEmail, tilEmail)) {
            return;
        }
        if (!validation.isEditTextFilled(etPassword, tilPassword)) {
            return;
        }

        mProgress.setMessage("Signing Up ...");
        mProgress.setCancelable(false);
        mProgress.show();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            String user_id = mAuth.getCurrentUser().getUid();

                            Log.i(TAG, "onComplete: user ID " + user_id);

                            mProgress.dismiss();
                            Toast.makeText(RegisterActivity.this, "Account created successful" + user_id + "yeah", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(RegisterActivity.this, HomeActivity.class));

                        } else {

                            Toast.makeText(RegisterActivity.this, "Account registration failed.", Toast.LENGTH_SHORT).show();
                        }
                        mProgress.dismiss();
                    }
                });


    }

    private void initView() {

        toolbar = findViewById(R.id.toolbar);

        etUsername = findViewById(R.id.etUsername);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);

        tilUsername = findViewById(R.id.tilUsername);
        tilEmail = findViewById(R.id.tilEmail);
        tilPassword = findViewById(R.id.tilPassword);

        btnSignUp = findViewById(R.id.btnSignUp);
    }

}
