package com.blindtech.sellme.api;

import com.blindtech.sellme.models.Account;
import com.blindtech.sellme.models.User;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 *
 */

public class AccountHelper {

    private static final String COLLECTION_NAME = "accounts";

    // --- COLLECTION REFERENCE ---

    public static CollectionReference getAccountCollection() {
        return FirebaseFirestore.getInstance().collection(COLLECTION_NAME);
    }

    // --- CREATE ---

    public static Task<Void> createAccount( String code, String amount, User user) {
        // 1 - Create Obj
        Account accountToCreate = new Account(code, amount, user);

        return AccountHelper.getAccountCollection().document().set(accountToCreate);
    }

    // --- GET ---

    public static Task<DocumentSnapshot> getAccount(String uid) {
        return AccountHelper.getAccountCollection().document(uid).get();
    }

    // --- UPDATE ---

    public static Task<Void> updateAccount(double amount, String uid) {
        return AccountHelper.getAccountCollection().document(uid).update("amount", amount);
    }

    // --- DELETE ---

    public static Task<Void> deleteAccount(String uid) {
        return AccountHelper.getAccountCollection().document(uid).delete();
    }

}
