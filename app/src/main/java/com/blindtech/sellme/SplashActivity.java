package com.blindtech.sellme;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.blindtech.sellme.activities.LoginActivity;

public class SplashActivity extends AppCompatActivity {

    boolean actif = true;
    int time = 2000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread splashThread = new Thread() {
            @Override
            public void run() {
                try {
                    int attente = 0;
                    while (actif && (attente < time)) {
                        sleep(100);
                        if (actif) {
                            attente += 100;
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(SplashActivity.this, "une erreur est survenue", Toast.LENGTH_SHORT).show();
                } finally {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }

            }
        };
        splashThread.start();
    }
}
