package com.blindtech.sellme.models;


/**
 *
 */
public class Account {

    private String code;
    private String amount;
    private User user;

    public Account() {
    }

    public Account(String code, String amount, User user) {
        this.code = code;
        this.amount = amount;
        this.user = user;
    }

    // --------------------
    //  GETTERS
    // --------------------

    public String getCode() {
        return code;
    }

    public String getAmount() {
        return amount;
    }

    public User getUser() {
        return user;
    }

    // --------------------
    //  SETTERS
    // --------------------

    public void setCode(String code) {
        this.code = code;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
