package com.blindtech.sellme.models;


public class Article {

    private String name;
    private String brand;
    private String categorie;
    private int prix;
    private int prod_image;
    private User user;
    private String users;


    public Article() {
    }

    public Article(String name, String brand, String categorie, int prix, int prod_image, String users) {
        this.name = name;
        this.brand = brand;
        this.categorie = categorie;
        this.prix = prix;
        this.prod_image = prod_image;
        this.users = users;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public int getprod_image() {
        return prod_image;
    }

    public void setprod_image(int prod_image) {
        this.prod_image = prod_image;
    }

    public String getUsers() {
        return users;
    }

    public void setUsers(String users) {
        this.users = users;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
