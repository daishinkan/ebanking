package com.blindtech.sellme.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blindtech.sellme.R;
import com.blindtech.sellme.activities.ArticleActivity;
import com.blindtech.sellme.activities.ProfilActivity;
import com.blindtech.sellme.helper.ItemClickListener;
import com.blindtech.sellme.models.Article;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.MyHolder> {

    private Context context;
    private List<Article> data;

    public ArticleAdapter(Context context, List<Article> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.article_item, viewGroup, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder myHolder, int i) {

        Article article = data.get(i);

        myHolder.produit_name.setText(article.getName());
        myHolder.brand_name.setText(article.getBrand());
//        myHolder.user_name.setText(article.getUser().getUser_name());
        myHolder.produit_price.setText(String.valueOf(article.getPrix()));

//        Picasso.get().load(article.getUser().getPhotoUrl()).into(myHolder.profil_image);
//        Picasso.get()
//                .load(article.getprod_image())
//                .into(myHolder.produit_foto);

        myHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

                Intent i = new Intent(context, ArticleActivity.class);

//                i.putExtra("",);
//                i.putExtra("",);
//                i.putExtra("",);

                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });

//        myHolder.produit_foto.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(context, ArticleActivity.class);
//
////                i.putExtra("",);
////                i.putExtra("",);
////                i.putExtra("",);
//
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(i);
//            }
//        });

        myHolder.user_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ProfilActivity.class));
            }
        });

    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    public class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        private TextView produit_name;
        private TextView brand_name;
        private TextView user_name;
        private TextView produit_price;
        private ImageView produit_foto;
        private ImageView profil_image;

        private ItemClickListener itemClickListener;
        private RelativeLayout relativeLayout;
        private LinearLayout linearLayout;

        public MyHolder(@NonNull View itemView) {
            super(itemView);

            produit_name = itemView.findViewById(R.id.product_name);
            user_name = itemView.findViewById(R.id.user_name);
            brand_name = itemView.findViewById(R.id.brand_name);
            produit_price = itemView.findViewById(R.id.product_price);
            produit_foto = itemView.findViewById(R.id.product_photo);
            profil_image = itemView.findViewById(R.id.profile_image);


            relativeLayout = itemView.findViewById(R.id.relativ_content);
            linearLayout = itemView.findViewById(R.id.linear_profil);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(v, getLayoutPosition());
        }

        public void setItemClickListener(ItemClickListener ic) {
            this.itemClickListener = ic;
        }
    }

}