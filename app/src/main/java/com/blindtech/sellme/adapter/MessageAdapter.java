package com.blindtech.sellme.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blindtech.sellme.R;
import com.blindtech.sellme.models.Message;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.mHholder> {

    private Context context;
    private List<Message> data;


    public MessageAdapter(Context context, List<Message> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public mHholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.message_item, viewGroup, false);
        return new mHholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull mHholder mHholder, int i) {

        Message msg =data.get(i);

        /*mHholder.name.setText(msg.getName());
        mHholder.contenu.setText(msg.getContenu());
        mHholder.date.setText( msg.getDate());*/

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class mHholder extends RecyclerView.ViewHolder {

        private TextView name, contenu, date;
        private ImageView profile;

        public mHholder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            contenu = itemView.findViewById(R.id.contenu);
            date = itemView.findViewById(R.id.date);
            profile = itemView.findViewById(R.id.profile);
        }
    }
}
