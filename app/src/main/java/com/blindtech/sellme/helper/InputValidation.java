package com.blindtech.sellme.helper;

import android.app.Activity;
import android.content.Context;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;

/**
 * Created by walker on 15/11/2018.
 */

public class InputValidation {

    Context context;

    public InputValidation(Context context) {
        this.context = context;
    }

    public boolean isEditTextFilled(EditText editText, TextInputLayout til) {
        String value = editText.getText().toString().trim();
        if (value.isEmpty()) {
            til.setError("Field can't be empty!");
            hideKeyboardFrom(editText);
            return false;
        } else {
            til.setError(null);
        }
        return true;
    }

    public boolean isEditTextEmail(EditText editText, TextInputLayout til) {
        String value = editText.getText().toString().trim();
        if (value.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(value).matches()) {
            til.setError("Enter Valid Email!");
            hideKeyboardFrom(editText);
            return false;
        } else {
            til.setError(null);
        }
        return true;
    }

    private void hideKeyboardFrom(View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        // assert imm != null;
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }
}